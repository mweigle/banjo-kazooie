#include <ultra64.h>
#include "functions.h"
#include "variables.h"

Actor *func_802CCA7C(ActorMarker *, Gfx **, Mtx **, Vtx **);
void func_802CCC5C(Actor *this);

/* .data */
extern ActorInfo D_80367130 = { 
    0xC2, 0x134, 0x3ED, 
    0, NULL, 
    func_802CCC5C, NULL, func_802CCA7C, 
    { 0x0, 0x0}, 0, 1.0f, { 0x0, 0x0, 0x0, 0x0}
};


/* .code */
#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_45310/func_802CC2A0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_45310/func_802CC340.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_45310/func_802CC4A4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_45310/func_802CC57C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_45310/func_802CC640.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_45310/func_802CC9FC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_45310/func_802CCA3C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_45310/func_802CCA7C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_45310/func_802CCBC8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_45310/func_802CCBF4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_45310/func_802CCC5C.s")
