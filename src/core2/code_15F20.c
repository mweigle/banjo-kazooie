#include <ultra64.h>
#include "functions.h"
#include "variables.h"


s32 func_8029CEB0(void){
    return func_802944F4();
}

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_15F20/func_8029CED0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_15F20/func_8029CF20.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_15F20/func_8029CF48.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_15F20/func_8029CF6C.s")
