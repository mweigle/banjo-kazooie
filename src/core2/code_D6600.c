#include <ultra64.h>
#include "functions.h"
#include "variables.h"

void func_8035DA1C(Actor *this);

/* .data */
extern ActorAnimationInfo D_80372EA0[];
extern ActorInfo D_80372EE0 = { 
    0x69, 0xA, 0x36B, 
    0x2, D_80372EA0, 
    func_8035DA1C, func_80326224, func_80325888, 
    { 0xB, 0xB8}, 0, 0.0f, { 0x0, 0x0, 0x0, 0x0}
};

/* .code */
#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_D6600/func_8035D590.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_D6600/func_8035D608.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_D6600/func_8035D65C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_D6600/func_8035D6FC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_D6600/func_8035D7CC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_D6600/func_8035D88C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_D6600/func_8035D8F0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_D6600/func_8035D95C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_D6600/func_8035DA1C.s")
